(function ($) {
    "use strict";

    //Menu On Hover Dropdown
    $('.primary-navigation').on('mouseenter mouseleave', '.nav-item', function (e) {
        if ($(window).width() > 750) {
            var _d = $(e.target).closest('.nav-item'); _d.addClass('show');
            setTimeout(function () {
                _d[_d.is(':hover') ? 'addClass' : 'removeClass']('show');
            }, 1);
        }
    });


    // Sticky Navigation
    $(function () {
        var header = $(".start-style");
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            if (scroll >= 200) {
                header.removeClass('start-style').addClass("scroll-on");
            } else {
                header.removeClass("scroll-on").addClass('start-style');
            }
        });
    });

    var featuredPosts = $(".feature-carousel");
    featuredPosts.owlCarousel({
        items: 1,
        nav: false,
        loop: true,
        dots: true,
        autoplay: false,
        smartSpeed: 450,
        animateOut: 'fadeOut',
    });

    $('.venobox').venobox({
        framewidth : '80%',                            // default: ''
        frameheight: '500px',                            // default: ''
        border     : '10px',                             // default: '0'
        bgcolor    : '#fff',                          // default: '#fff'
        titleattr  : 'data-title',                       // default: 'title'
        numeratio  : true,                               // default: false
        infinigall : true,                               // default: false
        
    }); 




})(jQuery);
